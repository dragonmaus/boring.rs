use atomicwrites::{AllowOverwrite, AtomicFile};
use realpath::realpath;
use std::{collections::HashSet, env, error::Error, fs, io, io::Write, path::PathBuf};

pub fn get_repo(name: &str) -> Result<PathBuf, Box<dyn Error>> {
    match fs::metadata(name) {
        Err(error) => {
            if realpath(".")? == realpath("..")? {
                return Err(io::Error::new(
                    error.kind(),
                    format!("unable to find {}: {}", name, error),
                )
                .into());
            }
        }
        Ok(metadata) => {
            if metadata.file_type().is_dir() {
                return Ok(realpath(name)?);
            }
        }
    }

    env::set_current_dir("..")?;
    get_repo(name)
}

pub struct Grammar {
    pub comments: Vec<&'static str>,
    pub negates: Vec<&'static str>,
}

pub fn update(file: PathBuf, args: Vec<String>, lang: Grammar) -> Result<i32, Box<dyn Error>> {
    let old = fs::read_to_string(&file).or_else(|e| {
        if e.kind() != io::ErrorKind::NotFound {
            return Err(e);
        }

        fs::File::create(&file)?;
        Ok("".into())
    })?;

    eprint!("Updating {}… ", file.to_string_lossy());
    let new = merge(&old, &args, lang)?;

    if new == old {
        eprintln!("Nothing to do!");
    } else {
        eprintln!("Done!");
        AtomicFile::new(&file, AllowOverwrite).write(|f| f.write_all(new.as_bytes()))?;
    }

    Ok(0)
}

fn merge(text: &str, args: &[String], lang: Grammar) -> Result<String, Box<dyn Error>> {
    let mut lines: HashSet<String> = text.lines().map(String::from).collect();

    for arg in args {
        lines.insert(arg.to_string());
    }

    let mut lines: Vec<String> = lines
        .into_iter()
        .filter_map(|line| {
            let line = line.trim().to_string();

            if line.is_empty() || lang.comments.iter().any(|s| line.starts_with(s)) {
                None
            } else {
                Some(line)
            }
        })
        .collect();

    let lines = lines.as_mut_slice();
    lines.sort_unstable();

    let mut lines = lines.to_vec();
    lines.dedup();

    if !lang.negates.is_empty() {
        let (neg, pos): (Vec<String>, Vec<String>) = lines
            .iter()
            .cloned()
            .partition(|l| lang.negates.iter().any(|s| l.starts_with(s)));
        lines.clear();
        lines.extend(pos);
        lines.extend(neg);
    }

    let mut text = lines.join("\n");
    if !text.is_empty() {
        text.push('\n');
    }

    Ok(text)
}
