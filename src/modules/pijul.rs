use crate::common;
use std::{env, error::Error, path::PathBuf};

pub enum Mode {
    File(String),
    Ignore,
    Repo,
}

pub fn update(mode: Mode, args: Vec<String>) -> Result<i32, Box<dyn Error>> {
    let file = get_file(mode)?;

    let grammar = common::Grammar {
        comments: vec!["#"],
        negates: vec!["!"],
    };

    common::update(file, args, grammar)
}

fn get_file(mode: Mode) -> Result<PathBuf, Box<dyn Error>> {
    match mode {
        Mode::File(name) => Ok(env::current_dir()?.join(name)),
        Mode::Ignore => Ok(common::get_repo(".pijul")?
            .parent()
            .unwrap()
            .join(".ignore")),
        Mode::Repo => Ok(common::get_repo(".pijul")?.join("local").join("ignore")),
    }
}
