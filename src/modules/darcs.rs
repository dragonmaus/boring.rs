use crate::common;
use std::{env, error::Error, fs, path::PathBuf};

pub enum Mode {
    Binaries,
    Boring,
    File(String),
}

pub fn update(mode: Mode, args: Vec<String>) -> Result<i32, Box<dyn Error>> {
    let file = get_file(mode)?;

    let grammar = common::Grammar {
        comments: vec!["--"],
        negates: vec![],
    };

    common::update(file, args, grammar)
}

fn get_file(mode: Mode) -> Result<PathBuf, Box<dyn Error>> {
    let file = match mode {
        Mode::Binaries => "binaries",
        Mode::Boring => "boring",
        Mode::File(name) => return Ok(env::current_dir()?.join(name)),
    };
    let repo = common::get_repo("_darcs")?;

    let prefs = repo.join("prefs").join("prefs");
    if let Ok(text) = fs::read_to_string(&prefs) {
        for line in text.lines() {
            let (key, value) = line.split_at(line.find(|c: char| c.is_whitespace()).unwrap_or(0));

            if key == format!("{}file", file) {
                return Ok(repo.parent().unwrap().join(value.trim_start()));
            }
        }
    }

    Ok(repo.join("prefs").join(&file))
}
