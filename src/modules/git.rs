use crate::common;
use git2::{Config, Error as GitError, ErrorClass, ErrorCode, Repository};
use std::{env, error::Error, path::PathBuf};

pub enum Mode {
    File(String),
    Global,
    Ignore,
    Repo,
}

pub fn update(mode: Mode, args: Vec<String>) -> Result<i32, Box<dyn Error>> {
    let file = get_file(mode)?;

    let grammar = common::Grammar {
        comments: vec!["#"],
        negates: vec!["!"],
    };

    common::update(file, args, grammar)
}

fn get_file(mode: Mode) -> Result<PathBuf, Box<dyn Error>> {
    match mode {
        Mode::File(name) => Ok(env::current_dir()?.join(name)),
        Mode::Global => global_ignore_file(),
        Mode::Ignore => local_ignore_file(),
        Mode::Repo => repo_ignore_file(),
    }
}

fn global_ignore_file() -> Result<PathBuf, Box<dyn Error>> {
    match Config::open_default()?.get_path("core.excludesFile") {
        Err(error) => {
            if error.class() == ErrorClass::Config && error.code() == ErrorCode::NotFound {
                Ok(xdg_config_home()?.join("git").join("ignore"))
            } else {
                Err(Box::new(error))
            }
        }
        Ok(path) => Ok(path),
    }
}

fn local_ignore_file() -> Result<PathBuf, Box<dyn Error>> {
    match Repository::open_from_env()?.workdir() {
        None => Err(Box::new(GitError::from_str("Repository is bare"))),
        Some(path) => Ok(path.join(".gitignore")),
    }
}

fn repo_ignore_file() -> Result<PathBuf, Box<dyn Error>> {
    Ok(Repository::open_from_env()?
        .path()
        .join("info")
        .join("exclude"))
}

fn xdg_config_home() -> Result<PathBuf, Box<dyn Error>> {
    if let Some(path) = dirs::config_dir() {
        return Ok(path);
    }

    if let Some(path) = dirs::home_dir() {
        return Ok(path.join(".config"));
    }

    Err(Box::new(GitError::from_str(
        "Could not find XDG_CONFIG_HOME",
    )))
}
