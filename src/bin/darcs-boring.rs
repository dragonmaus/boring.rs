use boring::modules::darcs::{update, Mode};
use getopt::prelude::*;
use my::{
    program_main,
    util::{program_args, program_name},
};
use std::error::Error;

program_main!("darcs-boring");

fn usage_line() -> String {
    format!(
        "Usage: {} [-bh] [-f FILE] pattern [pattern ...]",
        program_name("darcs-boring")
    )
}

fn print_usage() -> Result<i32, Box<dyn Error>> {
    println!("{}", usage_line());
    println!("  -b       operate on binariesfile");
    println!("  -f FILE  operate on FILE");
    println!();
    println!("  -h       display this help");
    Ok(0)
}

fn program() -> Result<i32, Box<dyn Error>> {
    let mut args = program_args();
    let mut opts = Parser::new(&args, "bf:h");
    let mut mode = Mode::Boring;

    loop {
        match opts.next().transpose()? {
            None => break,
            Some(opt) => match opt {
                Opt('b', None) => mode = Mode::Binaries,
                Opt('f', Some(arg)) => mode = Mode::File(arg),
                Opt('h', None) => return print_usage(),
                _ => unreachable!(),
            },
        }
    }

    let args = args.split_off(opts.index());

    if args.is_empty() {
        eprintln!("{}", usage_line());
        return Ok(1);
    }

    update(mode, args)
}
