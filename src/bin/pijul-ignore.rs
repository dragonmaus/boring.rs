use boring::modules::pijul::{update, Mode};
use getopt::prelude::*;
use my::{
    program_main,
    util::{program_args, program_name},
};
use std::error::Error;

program_main!("pijul-ignore");

fn usage_line() -> String {
    format!(
        "Usage: {} [-hr] [-f FILE] pattern [pattern ...]",
        program_name("pijul-ignore")
    )
}

fn print_usage() -> Result<i32, Box<dyn Error>> {
    println!("{}", usage_line());
    println!("  -f FILE  operate on FILE");
    println!("  -r       operate on internal repository ignore file");
    println!();
    println!("  -h       display this help");
    Ok(0)
}

fn program() -> Result<i32, Box<dyn Error>> {
    let mut args = program_args();
    let mut opts = Parser::new(&args, "f:hr");
    let mut mode = Mode::Ignore;

    loop {
        match opts.next().transpose()? {
            None => break,
            Some(opt) => match opt {
                Opt('f', Some(arg)) => mode = Mode::File(arg),
                Opt('h', None) => return print_usage(),
                Opt('r', None) => mode = Mode::Repo,
                _ => unreachable!(),
            },
        }
    }

    let args = args.split_off(opts.index());

    if args.is_empty() {
        eprintln!("{}", usage_line());
        return Ok(1);
    }

    update(mode, args)
}
